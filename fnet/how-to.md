# @flownet/lib-v8-build: Developer Guide

## Overview

The `@flownet/lib-v8-build` library is designed to streamline the process of building the V8 JavaScript engine for specific operating systems and architectures. Utilizing this library, developers can automate setting up the environment, fetching the V8 source code, and configuring the build process for creating platform-specific binaries. The library focuses on iOS and macOS platforms, supporting different CPUs and environments like simulators and physical devices.

## Installation

To incorporate `@flownet/lib-v8-build` into your project, you can use either npm or yarn. Execute the following command in your terminal:

```bash
# Using npm
npm install @flownet/lib-v8-build

# Using yarn
yarn add @flownet/lib-v8-build
```

## Usage

The library exports a single asynchronous function that handles the entire build process from start to finish. Here's how you can use it in your project:

### Example Usage

```javascript
import buildV8 from '@flownet/lib-v8-build';

async function build() {
  try {
    const buildOptions = {
      interactive: false,
      targetOs: 'ios', // options: 'ios', 'macos'
      targetCpu: 'arm64', // options: 'x64', 'arm64'
      targetEnv: 'device' // options: 'simulator', 'device'
    };

    const context = await buildV8(buildOptions);
    console.log("Build completed successfully:", context);
  } catch (error) {
    console.error("An error occurred during the build process:", error);
  }
}

build();
```

In this example, the `buildV8` function is invoked with specific options to build the V8 engine for an iOS device running on an arm64 CPU.

## Examples

### Basic iOS Build

```javascript
import buildV8 from '@flownet/lib-v8-build';

async function buildIOS() {
  const options = {
    targetOs: 'ios',
    targetCpu: 'arm64',
    targetEnv: 'device'
  };
  
  const context = await buildV8(options);
  console.log(context);
}

buildIOS();
```

### macOS Simulator Build

```javascript
import buildV8 from '@flownet/lib-v8-build';

async function buildMacOSSimulator() {
  const options = {
    targetOs: 'macos',
    targetCpu: 'x64',
    targetEnv: 'simulator'
  };

  const context = await buildV8(options);
  console.log(context);
}

buildMacOSSimulator();
```

These examples demonstrate how to configure builds for different OS and CPU environments, providing flexibility depending on the specific requirements of your application.

## Acknowledgement

The `@flownet/lib-v8-build` library utilizes the `@fnet/list-files` package for managing file operations during the build process. This acknowledgement ensures compliance with any external contributions or dependencies integrated into the library.