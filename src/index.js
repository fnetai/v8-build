import shell from 'shelljs';
import path from 'node:path';
import fs from 'node:fs';
import { spawn } from 'node:child_process';
import listFiles from "@fnet/list-files";

import prepareDepotTools from '@flownet/lib-v8-build-env-depot-tools';

export default async ({
  interactive = false,
  // universal = false,
  targetOs, // ios || macos
  targetCpu, // x64 || arm64
  targetEnv // simulator || device
}) => {
  // Prepare depot_tools
  const depotToolsEnv = prepareDepotTools();

  const V8_WORKSPACE_ROOT = depotToolsEnv.V8_WORKSPACE_ROOT;
  const V8_SOURCE_DIR = path.join(V8_WORKSPACE_ROOT, 'v8');

  let context = {
    target_os: targetOs,
    target_cpu: targetCpu,
    target_environment: targetEnv,
    V8_SOURCE_DIR,
    V8_WORKSPACE_ROOT
  }

  context = await fetchV8(context);

  if (interactive) return await startInteractiveShell(context);
  // else if (universal) return await createAppleUniversal(context);

  context = await createGN(context);
  context = await buildGN(context);
  context = await publishToDist(context);

  return context;
};

async function fetchV8({ V8_WORKSPACE_ROOT, V8_SOURCE_DIR }) {
  let context = arguments[0];

  // Check if V8 source directory already exists
  if (!shell.test('-d', V8_SOURCE_DIR)) {
    // Navigate to workspace root
    shell.cd(V8_WORKSPACE_ROOT);

    console.log("Fetching V8 source code...");

    // Fetch V8 source
    if (shell.exec('fetch v8').code !== 0) {
      throw new Error('Error encountered during fetching V8 source code.');
    }

    // After fetching, run gclient sync to sync dependencies
    console.log("Running gclient sync...");
    if (shell.exec('gclient sync').code !== 0) {
      throw new Error('Error encountered during gclient sync after fetching V8.');
    }
  } else {
    console.log("V8 source directory detected. Running gclient sync to update dependencies...");

    // Even if the source directory exists, run gclient sync to ensure everything is up-to-date
    shell.cd(V8_SOURCE_DIR);
    if (shell.exec('gclient sync').code !== 0) {
      throw new Error('Error encountered during gclient sync for existing V8 setup.');
    }
  }

  return context;
}

async function startInteractiveShell({ V8_SOURCE_DIR }) {
  return new Promise((resolve, reject) => {
    const interactiveShell = spawn('zsh', ['-i'], { stdio: 'inherit', cwd: V8_SOURCE_DIR });

    interactiveShell.on('exit', (code) => {
      if (code === 0) {
        console.log("Shell session ended.");
        resolve();
      } else {
        reject(new Error(`Shell exited with code: ${code}`));
      }
    });
  });
}

async function createGN({ V8_SOURCE_DIR }) {

  let context = arguments[0];

  shell.cd(V8_SOURCE_DIR);

  context = createGNArgs(context);

  const args = context.args;
  const argsFileContent = args.join('\n');

  const buildName = `${context.target_os}-${context.target_cpu}-${context.target_environment}`;
  const buildDir = path.join(V8_SOURCE_DIR, 'out', buildName);

  const argsGnFilePath = path.join(buildDir, 'args.gn');

  let updated = false;

  if (fs.existsSync(argsGnFilePath)) {
    fs.writeFileSync(path.join(buildDir, 'args.gn'), argsFileContent, { encoding: 'utf8' });
    updated = true;
  }

  if (shell.exec(`gn gen out/${buildName}`).code !== 0) throw new Error('Error encountered during GN args setting.');

  if (!updated) fs.writeFileSync(path.join(buildDir, 'args.gn'), argsFileContent, { encoding: 'utf8' });

  const V8_BUILD_PATH = path.join(V8_SOURCE_DIR, 'out', buildName);

  return { ...context, buildName, buildDir, V8_BUILD_PATH };
}

function createGNArgs({ target_os }) {
  let context = arguments[0];
  let args = [];

  context = { ...context, args };

  context = createGNBaseArgs(context);

  if (target_os === 'ios' || target_os === 'macos') context = createGNAppleArgs(context);
  else throw new Error(`Unsupported target OS: ${target_os}`);

  return context;
}

function createGNBaseArgs({ args, target_os, target_cpu, target_environment }) {
  let context = arguments[0];

  args.push(`target_cpu = "${target_cpu}"`);
  args.push(`target_os = "${target_os}"`);
  args.push(`target_environment = "${target_environment}"`);

  args.push('is_debug = false');
  args.push('use_custom_libcxx = false');

  args.push('v8_monolithic = true');
  args.push('v8_use_external_startup_data = false');
  args.push('v8_enable_i18n_support = false');

  return context;
}

function createGNAppleArgs({ args, target_os }) {
  let context = arguments[0];

  if (target_os === 'ios') context = createGNIOSArgs(context);
  else if (target_os === 'macos') context = createGNMacArgs(context);
  else throw new Error(`Unsupported target OS: ${target_os}`);

  return context;
}

function createGNIOSArgs({ args, target_environment }) {
  let context = arguments[0];

  args.push(`ios_deployment_target = "14"`);
  args.push('v8_symbol_level = 0');

  if (target_environment === 'simulator') context = createGNIOSSimulatorArgs(context);
  else if (target_environment === 'device') context = createGNIOSDeviceArgs(context);
  else throw new Error(`Unsupported target environment: ${target_environment}`);

  return context;
}

function createGNIOSSimulatorArgs({ args, target_environment, target_cpu }) {
  let context = arguments[0];

  if (!['x64', 'arm64'].includes(target_cpu)) throw new Error(`Unsupported target CPU for simulator: ${target_cpu}`);

  args.push(`ios_enable_code_signing = false`);

  return context;
}

function createGNIOSDeviceArgs({ args, target_cpu }) {
  let context = arguments[0];

  if (!['arm64'].includes(target_cpu)) throw new Error(`Unsupported target CPU for device: ${target_cpu}`);

  args.push(`ios_enable_code_signing = false`);

  return context;
}

function createGNMacArgs({ args }) {
  let context = arguments[0];

  args.push(`mac_deployment_target = "13"`);
  args.push('v8_symbol_level = 0');

  // args.push('is_component_build = false');
  // args.push('v8_enable_pointer_compression = false');
  // args.push('v8_enable_31bit_smis_on_64bit_arch = false');

  return context;
}

async function publishToDist({ V8_WORKSPACE_ROOT, V8_BUILD_PATH, target_os, target_cpu, target_environment }) {
  const context = arguments[0];

  const objDir = path.join(V8_BUILD_PATH, 'obj');

  const libraryFiles = await listFiles({ dir: objDir, pattern: '*.a' });

  let applePlatformName;
  let appleArchName;
  if (target_os === 'ios') {
    if (target_environment === 'simulator') {
      applePlatformName = 'iphonesimulator';
    }
    else if (target_environment === 'device') {
      applePlatformName = 'iphoneos';
    }
    else throw new Error(`Unsupported target environment: ${target_environment}`);

    if (target_cpu === 'arm64') appleArchName = 'arm64';
    else if (target_cpu === 'x64') appleArchName = 'x86_64';
    else throw new Error(`Unsupported target CPU for iOS: ${target_cpu}`);
  }
  else if (target_os === 'macos') {
    applePlatformName = 'macosx';

    if (target_cpu === 'arm64') appleArchName = 'arm64';
    else if (target_cpu === 'x64') appleArchName = 'x86_64';
    else throw new Error(`Unsupported target CPU for iOS: ${target_cpu}`);
  }

  const targetDistDir = path.join(V8_WORKSPACE_ROOT, 'dist', applePlatformName, appleArchName);

  if (shell.test('-d', targetDistDir)) {
    if (shell.rm('-rf', targetDistDir).code !== 0)
      throw new Error(`Error encountered during removing target dist directory: ${targetDistDir}`);
  }

  let shellRes = shell.mkdir('-p', targetDistDir);
  if (shellRes.code !== 0) throw new Error(`Error encountered during creating target dist directory: ${targetDistDir}`);

  for (const relLibFile of libraryFiles) {
    const fullLibFile = path.join(objDir, relLibFile);
    const targetLibraryFile = path.join(targetDistDir, relLibFile);
    let shellRes = shell.cp(fullLibFile, targetLibraryFile);
    if (shellRes.code !== 0) throw new Error(`Error encountered during copying library file: ${fullLibFile}`);
  }

  const v8BuildConfig = await getBuildConfig(context);

  let envFileContent = [];
  envFileContent.push(`HEADER_SEARCH_PATHS = $(HOME)/v8/v8/include`);
  envFileContent.push(`LIBRARY_SEARCH_PATHS = $(HOME)/v8/dist/$(PLATFORM_NAME)/$(NATIVE_ARCH)`);
  envFileContent.push(`OTHER_LINK_FLAGS = ${libraryFiles.map(lib => `-l${path.basename(lib, '.a').substring(3)}`).join(' ')}`);

  const cxxFlags = [];
  if (v8BuildConfig.sandbox) cxxFlags.push('-DV8_ENABLE_SANDBOX');
  if (v8BuildConfig.pointer_compression) cxxFlags.push('-DV8_COMPRESS_POINTERS');
  envFileContent.push(`OTHER_CXX_FLAGS = ${cxxFlags.join(' ')}`);

  fs.writeFileSync(path.join(targetDistDir, 'xcode-settings.txt'), envFileContent.join('\n'), { encoding: 'utf8' });

  return { ...context, v8BuildConfig };
}
async function getBuildConfig({ V8_BUILD_PATH }) {
  const context = arguments[0];

  const files = await listFiles({ dir: V8_BUILD_PATH, pattern: ['v8_build_config.json', '*/v8_build_config.json'] });
  if (!files.length) return;

  const file = files[0];

  const v8BuildConfig = JSON.parse(fs.readFileSync(path.join(V8_BUILD_PATH, file), { encoding: 'utf8' }));

  return v8BuildConfig;
}

async function buildGN({ buildName }) {
  const context = arguments[0];

  if (shell.exec(`ninja -C out/${buildName} v8_monolith`).code !== 0) {
    throw new Error(`Error encountered during building V8 for ${buildName}.`);
  }

  return context;
}

// async function createAppleUniversal({ V8_WORKSPACE_ROOT, target_os }) {
//     const context = arguments[0];

//     const distDir = path.join(V8_WORKSPACE_ROOT, 'dist', target_os);
//     const universalDistDir = path.join(distDir, `universal`);

//     if (shell.test('-d', universalDistDir)) {
//         if (shell.rm('-rf', universalDistDir).code !== 0)
//             throw new Error(`Error encountered during removing universal dist directory: ${universalDistDir}`);
//     }
//     shell.mkdir('-p', universalDistDir);

//     const libFiles = await listFiles({ dir: distDir, pattern: '*/*.a' });

//     const groupedLibs = {};
//     libFiles.forEach(lib => {
//         const libName = path.basename(lib);
//         if (!groupedLibs[libName]) {
//             groupedLibs[libName] = [];
//         }
//         groupedLibs[libName].push(path.join(distDir, lib));
//     });

//     for (const [libName, architectures] of Object.entries(groupedLibs)) {
//         const outputLibPath = path.join(universalDistDir, libName);
//         const lipoCmd = `lipo -create ${architectures.join(' ')} -output ${outputLibPath}`;
//         const result = shell.exec(lipoCmd);
//         if (result.code !== 0) {
//             throw new Error(`Error encountered during creating universal library for ${libName}`);
//         }
//     }

//     console.log(`Universal libraries created at: ${universalDistDir}`);

//     return context;
// }